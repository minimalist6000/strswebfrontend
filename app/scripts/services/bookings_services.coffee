'use strict'

app = angular.module('mdemoApp')

# Synchronous communication  (CRUD interface)
app.service 'BookingsService', ($resource) ->
  #$resource 'http://172.19.0.129:3000/orders', {}
  #$resource 'http://strs-team1.herokuapp.com/orders', {}
  $resource 'http://localhost:3000/orders', {}


# Asynchronous communication (via Pusher)
app.service 'BookingsPusherService', ($rootScope) ->
  pusher = new Pusher('aee2891b234084cfc01e')
  channel = pusher.subscribe('bookings')
  onMessage: (callback) ->
    channel.bind 'async_notification', (data) ->
      $rootScope.$apply ->
        callback(data.message)

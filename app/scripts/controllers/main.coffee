'use strict'

angular.module('mdemoApp')
app = angular.module('mdemoApp')
app.controller 'MainCtrl', ($scope, BookingsService, BookingsPusherService) ->
  $scope.kalapea = 'loll'
  $scope.sync_notification = ''
  $scope.async_notification = ''

  
  $scope.submit = ->
    request = {start_location: 'Liivi 2, Tartu, Estonia'}
    BookingsService.save(request, (response) ->
      $scope.sync_notification = response.message
    )

  BookingsPusherService.onMessage (data) ->
    $scope.async_notification = data
